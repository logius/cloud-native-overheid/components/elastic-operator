# Elastic Cloud for K8s (Elactic operator)

## Deploying

The role deploys the operator in the __<platform-prefix>-elastic-operator__ namespace, using the k8s object definitions provided by the [quickstart](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html)

## Caveats

The quickstart uses __elastic-system__ as the namespace, which has been replaced. The method of replacement is not the best, and honestly neither is the usage of one big object file to install all the resources.
